#define DEBUG

//#define LED_TEST

#ifdef DEBUG
#define DEBUG_PRINTLN(x) Serial.println (x)
#define DEBUG_PRINT(x) Serial.print (x)
#define DEBUG_PRINTBIN(x) Serial.print(x, BIN)
#else
#define DEBUG_PRINTLN(x)
#define DEBUG_PRINT(x)
#define DEBUG_PRINTBIN(x)
#endif

#include <EEPROM.h>
#include <BasicStepperDriver.h>
#include <MultiDriver.h>
#include <SyncDriver.h>
#include <elapsedMillis.h>
#include <Bounce2.h>
#include "Rotary.h"
#include <Nextion.h>
#include "Color.h"

#define LEDS 0

#define X_ENCODER_A_PIN 2
#define X_ENCODER_B_PIN 3
#define X_ENCODER_KEY_PIN 16
#define LED_X 1
#define X_ENCODER_LED_R_PIN 6
#define X_ENCODER_LED_G_PIN 5
#define X_ENCODER_LED_B_PIN 4

#define Y_ENCODER_A_PIN 14
#define Y_ENCODER_B_PIN 15
#define Y_ENCODER_KEY_PIN 11
#define LED_Y 2
#define Y_ENCODER_LED_R_PIN 10
#define Y_ENCODER_LED_G_PIN 9
#define Y_ENCODER_LED_B_PIN 23

#define Y_STEPPER_STEP_PIN 18
#define Y_STEPPER_DIRECTION_PIN 17
#define Y_STEPPER_ENABLE_PIN 19
#define X_STEPPER_STEP_PIN 21
#define X_STEPPER_DIRECTION_PIN 20
#define X_STEPPER_ENABLE_PIN 22

#define STEPPER_MICROSTEPS 4

enum stepperMode
{
	SINGLE,
	SINGLEWAIT,
	MULTI,
	MULTIWAIT
};

Bounce x_key = Bounce(X_ENCODER_KEY_PIN, 5);
Bounce y_key = Bounce(Y_ENCODER_KEY_PIN, 5);
Rotary x_enc = Rotary(X_ENCODER_A_PIN, X_ENCODER_B_PIN);
Rotary y_enc = Rotary(Y_ENCODER_A_PIN, Y_ENCODER_B_PIN);

volatile long x_encPos = 0;
long x_oldEncPos = 0;
long x_diffEncPos = 0;

volatile long y_encPos = 0;
long y_oldEncPos = 0;
long y_diffEncPos = 0;

short rpm = 100;

long xpos = 0;
long ypos = 0;
long xpos_new = 0;
long ypos_new = 0;

bool updateNextionPositions = false;
bool tie_encoders_to_steppers = false;

elapsedMillis blinky;
unsigned int ledInterval = 500;
char xblink = -1; //-1 dont blink, 0 blink off, 1 blink on
char yblink = -1; //-1 dont blink, 0 blink off, 1 blink on

unsigned int stepSize = 4; // equals 0.01mm

BasicStepperDriver x_stepper(200, X_STEPPER_DIRECTION_PIN, X_STEPPER_STEP_PIN, X_STEPPER_ENABLE_PIN);
BasicStepperDriver y_stepper(200, Y_STEPPER_DIRECTION_PIN, Y_STEPPER_STEP_PIN, Y_STEPPER_ENABLE_PIN);
MultiDriver steppers(x_stepper, y_stepper);
stepperMode currentStepperMode = SINGLEWAIT;

char nexBuffer[100] = { 0 };
uint32_t nexNumberBuffer = 0;

/* Nextion page ID 1 (Menu) objects */
NexButton nex_menu_manual = NexButton(1, 3, "b1");
NexButton nex_menu_radial = NexButton(1, 1, "b0");
NexButton nex_menu_encoders = NexButton(1, 4, "b2");


/* Nextion page ID 2 (Manual) objects */
NexText nex_xpos = NexText(2, 21, "xpos");
NexText nex_ypos = NexText(2, 22, "ypos");
NexNumber nex_fr = NexNumber(2, 24, "fr");
NexButton nex_bXp = NexButton(2, 10, "bXp");
NexButton nex_bXm = NexButton(2, 11, "bXm");
NexButton nex_bYp = NexButton(2, 8, "bYp");
NexButton nex_bYm = NexButton(2, 9, "bYm");
NexButton nex_bHome = NexButton(2, 12, "bHome");
NexButton nex_bSetHome = NexButton(2, 23, "bSetHome");
NexButton nex_bGoto = NexButton(2, 13, "bGoto");
NexButton nex_frp = NexButton(2, 3, "frp");
NexButton nex_frm = NexButton(2, 2, "frm");
NexText nex_xGoto = NexText(2, 20, "xGoto");
NexText nex_yGoto = NexText(2, 19, "yGoto");
NexVariable nex_stepSize = NexVariable(2, 16, "stepSize");
NexButton nex_bStepp = NexButton(2, 14, "bSstepp");
NexButton nex_bStepm = NexButton(2, 15, "bStepm");
NexDSButton nex_benc = NexDSButton(2, 25, "benc");

// Register button objects to the touch event list.  
NexTouch *nex_listen_list[] = {
	&nex_bXp,
	&nex_bXm,
	&nex_bYp,
	&nex_bYm,
	&nex_bHome,
	&nex_bSetHome,
	&nex_bGoto,
	&nex_bStepp,
	&nex_bStepm,
	&nex_frp,
	&nex_frm,
	&nex_benc,
	NULL
};

/* Nextion callbacks */
void nex_bXpPopCallback(void *ptr) {
	DEBUG_PRINT(F("xpos+:"));
	if (x_stepper.getCurrentState() == BasicStepperDriver::STOPPED) {
		xpos_new = stepSize;
		currentStepperMode = MULTI;
	}
	else {
		xpos_new += stepSize;
	}
	DEBUG_PRINTLN(xpos_new);
}
void nex_bXmPopCallback(void *ptr) {
	DEBUG_PRINT(F("xpos-:"));
	if (x_stepper.getCurrentState() == BasicStepperDriver::STOPPED) {
		xpos_new = stepSize * -1;
		currentStepperMode = MULTI;
	}
	else {
		xpos_new -= stepSize;
	}
	DEBUG_PRINTLN(xpos_new);
}
void nex_bYpPopCallback(void *ptr) {
	DEBUG_PRINT(F("ypos+:"));
	if (y_stepper.getCurrentState() == BasicStepperDriver::STOPPED) {
		ypos_new = stepSize;
		currentStepperMode = MULTI;
	}
	else {
		ypos_new += stepSize;
	}
	DEBUG_PRINTLN(ypos_new);
}
void nex_bYmPopCallback(void *ptr) {
	DEBUG_PRINT(F("ypos-:"));
	if (y_stepper.getCurrentState() == BasicStepperDriver::STOPPED) {
		ypos_new = stepSize * -1;
		currentStepperMode = MULTI;
	}
	else {
		ypos_new -= stepSize;
	}
	DEBUG_PRINTLN(ypos_new);
}
void nex_bHomePopCallback(void *ptr) {
	DEBUG_PRINTLN(F("home"));
	if (xpos != 0) xpos_new = xpos * -1;
	if (ypos != 0) ypos_new = ypos * -1;
	currentStepperMode = MULTIWAIT;
}
void nex_bSetHomePopCallback(void *ptr) {
	DEBUG_PRINTLN(F("sethome"));
	xpos = 0;
	xpos_new = 0;
	ypos = 0;
	ypos_new = 0;
	updateNextionPositions = true;
	currentStepperMode = MULTI;
}
void nex_bGotoPopCallback(void *ptr) {
	DEBUG_PRINT(F("goto:"));
	memset(nexBuffer, 0, sizeof(nexBuffer));
	nex_xGoto.getText(nexBuffer, 10);
	float xg = atoff(nexBuffer);
	DEBUG_PRINT(xg);
	DEBUG_PRINT(F(","));
	memset(nexBuffer, 0, sizeof(nexBuffer));
	nex_yGoto.getText(nexBuffer, 10);
	float yg = atoff(nexBuffer);
	DEBUG_PRINTLN(yg);

	xpos_new = xg * 400;
	ypos_new = yg * 400;
	
	currentStepperMode = MULTIWAIT;
}
void nex_bStepPopCallback(void *ptr) {
	DEBUG_PRINT(F("stepsize"));
	nex_stepSize.getValue(&nexNumberBuffer);
	DEBUG_PRINTLN(nexNumberBuffer);

	setStepSize((byte)nexNumberBuffer);
}
void nex_frPopCallback(void *ptr) {
	DEBUG_PRINT(F("feedrate:"));
	nex_fr.getValue(&nexNumberBuffer);
	DEBUG_PRINTLN(nexNumberBuffer);
	rpm = (short)nexNumberBuffer;
	x_stepper.setRPM(rpm);
	y_stepper.setRPM(rpm);
}
void nex_bencPopCallback(void *ptr) {
	DEBUG_PRINT(F("encoder"));
	nex_benc.getValue(&nexNumberBuffer);
	DEBUG_PRINTLN(nexNumberBuffer);
	if (nexNumberBuffer == 0) {
		tie_encoders_to_steppers = false;
		currentStepperMode = MULTI;
	}
	else {
		tie_encoders_to_steppers = true;
		currentStepperMode = SINGLEWAIT;
	}

}
#ifdef LED_TEST
elapsedMillis colorTimer;
unsigned int colorInterval = 50;
byte currentColor = 0;
color colors[] = {LED_RED, LED_ORANGE, LED_YELLOW, LED_LIME, LED_GREEN, LED_AQUAMARINE, LED_CYAN, LED_DEEPSKYBLUE, LED_BLUE, LED_PURPLE, LED_MAGENTA, LED_FUSCIA};
#endif

void setup()
{
#ifdef DEBUG
	Serial.begin(115200);
#endif //DEBUG

	/* Init pinmodes */
	pinMode(X_ENCODER_KEY_PIN, INPUT_PULLUP);
	pinMode(X_ENCODER_LED_R_PIN, OUTPUT);
	pinMode(X_ENCODER_LED_G_PIN, OUTPUT);
	pinMode(X_ENCODER_LED_B_PIN, OUTPUT);

	pinMode(Y_ENCODER_KEY_PIN, INPUT_PULLUP);
	pinMode(Y_ENCODER_LED_R_PIN, OUTPUT);
	pinMode(Y_ENCODER_LED_G_PIN, OUTPUT);
	pinMode(Y_ENCODER_LED_B_PIN, OUTPUT);

	pinMode(X_STEPPER_STEP_PIN, OUTPUT);
	pinMode(X_STEPPER_DIRECTION_PIN, OUTPUT);
	pinMode(X_STEPPER_ENABLE_PIN, OUTPUT);
	pinMode(Y_STEPPER_STEP_PIN, OUTPUT);
	pinMode(Y_STEPPER_DIRECTION_PIN, OUTPUT);
	pinMode(Y_STEPPER_ENABLE_PIN, OUTPUT);

	setColor(LEDS, color(0,0,0));

	pinMode(X_ENCODER_A_PIN, INPUT_PULLUP);
	pinMode(X_ENCODER_B_PIN, INPUT_PULLUP);
	attachInterrupt(digitalPinToInterrupt(X_ENCODER_A_PIN), x_enc_rotate, CHANGE);
	attachInterrupt(digitalPinToInterrupt(X_ENCODER_B_PIN), x_enc_rotate, CHANGE);

	pinMode(Y_ENCODER_A_PIN, INPUT_PULLUP);
	pinMode(Y_ENCODER_B_PIN, INPUT_PULLUP);
	attachInterrupt(digitalPinToInterrupt(Y_ENCODER_A_PIN), y_enc_rotate, CHANGE);
	attachInterrupt(digitalPinToInterrupt(Y_ENCODER_B_PIN), y_enc_rotate, CHANGE);

/* Nextion */
	Serial1.begin(9600);
	nexInit();

	nex_bXp.attachPop(nex_bXpPopCallback, &nex_bXp);
	nex_bXm.attachPop(nex_bXmPopCallback, &nex_bXm);
	nex_bYp.attachPop(nex_bYpPopCallback, &nex_bYp);
	nex_bYm.attachPop(nex_bYmPopCallback, &nex_bYm);
	nex_bHome.attachPop(nex_bHomePopCallback, &nex_bHome);
	nex_bSetHome.attachPop(nex_bSetHomePopCallback, &nex_bSetHome);
	nex_bGoto.attachPop(nex_bGotoPopCallback, &nex_bGoto);
	nex_bStepp.attachPop(nex_bStepPopCallback, &nex_bStepp);
	nex_bStepm.attachPop(nex_bStepPopCallback, &nex_bStepm);
	nex_frp.attachPop(nex_frPopCallback, &nex_frp);
	nex_frm.attachPop(nex_frPopCallback, &nex_frm);
	nex_benc.attachPop(nex_bencPopCallback, &nex_benc);

	nex_fr.setValue(rpm);

#ifdef LED_TEST
	colorTimer = 0;
#endif

	/* init stepper */
	x_stepper.begin(rpm, STEPPER_MICROSTEPS);
	y_stepper.begin(rpm, STEPPER_MICROSTEPS);
	stepperEnable(0);

	blinky = 0;
}

void loop()
{
	if (x_encPos != x_oldEncPos) {
		x_diffEncPos = x_oldEncPos - x_encPos;

		if (tie_encoders_to_steppers) {
			if (x_stepper.getCurrentState() == BasicStepperDriver::STOPPED) {
				xpos_new = stepSize * x_diffEncPos;
			}
			else {
				xpos_new += (stepSize * x_diffEncPos);
			}
		}

		x_oldEncPos = x_encPos;
	}
	
	if (y_encPos != y_oldEncPos) {

		y_diffEncPos = y_oldEncPos - y_encPos;

		if (tie_encoders_to_steppers) {
			if (y_stepper.getCurrentState() == BasicStepperDriver::STOPPED) {
				ypos_new = stepSize * y_diffEncPos;
}
			else {
				ypos_new += (stepSize * y_diffEncPos);
			}
		}

		y_oldEncPos = y_encPos;
	}

#ifdef LED_TEST
	if (colorTimer >= colorInterval) {
		currentColor++;
		if (currentColor == 12) currentColor = 0;
		setColor(LED_X, colors[currentColor]);
		setColor(LED_Y, colors[12 - currentColor]);
		colorTimer = 0;
	}
#endif

	// manage buttons
	x_key.update();
	y_key.update();

	if (x_key.fell()) {
		DEBUG_PRINTLN(F("x_key pressed"));
	}

	if (y_key.fell()) {
		DEBUG_PRINTLN(F("y_key pressed"));
	}

	stepperControl();

	steppers.nextAction();

	if ( updateNextionPositions ) {
		// Expansive call, only update screen when steppers are not running
		if (x_stepper.getCurrentState() == BasicStepperDriver::STOPPED && y_stepper.getCurrentState() == BasicStepperDriver::STOPPED) {
			nex_updatePosition(true, true);
			xblink = -1;
			yblink = -1;
			updateNextionPositions = false;
		}
	}

	if (blinky >= ledInterval) {
		if (xblink == 1) {
			setColor(LED_X, color(0, 0, 0));
			xblink = 0;
		}
		else if (xblink == 0) {
			setColor(LED_X, LED_ORANGE);
			xblink = 1;
		}
		else {
			setColor(LED_X, LED_GREEN);
		}

		if (yblink == 1) {
			setColor(LED_Y, color(0, 0, 0));
			yblink = 0;
		}
		else if (yblink == 0) {
			setColor(LED_Y, LED_ORANGE);
			yblink = 1;
		}
		else {
			setColor(LED_Y, LED_GREEN);
		}

		blinky = 0;
	}

	nexLoop(nex_listen_list);
}

void stepperControl() {
	switch(currentStepperMode) {
	case SINGLE:
	case MULTI:
		if (xpos_new != 0 && x_stepper.getCurrentState() == BasicStepperDriver::STOPPED) {
			setColor(LED_X, LED_ORANGE);
			xblink = 1;
			x_stepper.startMove(xpos_new);
			xpos += xpos_new;
			xpos_new = 0;
			updateNextionPositions = true;
		}
		if (ypos_new != 0 && y_stepper.getCurrentState() == BasicStepperDriver::STOPPED) {
			setColor(LED_Y, LED_ORANGE);
			yblink = 1;
			y_stepper.startMove(ypos_new);
			ypos += ypos_new;
			ypos_new = 0;
			updateNextionPositions = true;
		}
		break;
	case SINGLEWAIT:
		if (xpos_new != 0 && x_stepper.getCurrentState() == BasicStepperDriver::STOPPED) {
			setColor(LED_X, LED_BLUE);
			x_stepper.move(xpos_new);
			xpos += xpos_new;
			xpos_new = 0;
			setColor(LED_X, LED_GREEN);
			updateNextionPositions = true;
		}
		if (ypos_new != 0 && y_stepper.getCurrentState() == BasicStepperDriver::STOPPED) {
			setColor(LED_Y, LED_BLUE);
			y_stepper.move(ypos_new);
			ypos += ypos_new;
			ypos_new = 0;
			setColor(LED_Y, LED_GREEN);
			updateNextionPositions = true;
		}
		break;
	case MULTIWAIT:
		if ((xpos_new != 0 || ypos_new != 0) && steppers.isRunning() == false) {
			setColor(LEDS, LED_PURPLE);
			steppers.move(xpos_new, ypos_new);
			xpos += xpos_new;
			ypos += ypos_new;
			xpos_new = 0;
			ypos_new = 0;
			setColor(LEDS, LED_GREEN);
			updateNextionPositions = true;
		}
		break;
	}
}

void stepperEnable(byte idx) {
	switch (idx) {
	case 1:
		x_stepper.enable();
		setColor(LED_X, LED_GREEN);
		break;
	case 2:
		y_stepper.enable();
		setColor(LED_Y, LED_GREEN);
		break;
	default:
		steppers.enable();
		setColor(LED_X, LED_GREEN);
		setColor(LED_Y, LED_GREEN);
	}
}

void stepperDisable(byte idx) {
	switch (idx) {
	case 1:
		x_stepper.disable();
		setColor(LED_X, LED_RED);
		break;
	case 2:
		y_stepper.disable();
		setColor(LED_Y, LED_RED);
		break;
	default:
		steppers.disable();
		setColor(LED_X, LED_RED);
		setColor(LED_Y, LED_RED);
	}
}

void setStepSize(byte number) {
	switch (number) {
	case 0:
		stepSize = 4; // 0.01mm
		break;
	case 1:
		stepSize = 20; // 0.05mm
		break;
	case 2:
		stepSize = 40; // 0.10mm
		break;
	case 3:
		stepSize = 200; // 0.50mm
		break;
	case 4:
		stepSize = 400; // 1.00mm
		break;
	case 5:
		stepSize = 2000; // 5.00mm
		break;
	case 6:
		stepSize = 4000; // 10.00mm
		break;
	default:
		stepSize = 20000; // 50.00mm
	}
}

void nex_updatePosition(bool doX, bool doY) {
	float lfxpos, lfypos;

	if (doX) {
		if (xpos != 0) {
			lfxpos = xpos / 400.0;
		}
		else {
			lfxpos = 0.0;
		}
		memset(nexBuffer, 0, sizeof(nexBuffer));
		dtostrf(lfxpos, 9, 2, nexBuffer);
		nex_xpos.setText(nexBuffer);
	}

	if (doY) {
		if (ypos != 0) {
			lfypos = ypos / 400.0;
		}
		else {
			lfypos = 0.0;
		}
		memset(nexBuffer, 0, sizeof(nexBuffer));
		dtostrf(lfypos, 9, 2, nexBuffer);
		nex_ypos.setText(nexBuffer);
	}
}

void setColor(byte led, color c) {
	switch (led) {
	case LED_X:
		analogWrite(X_ENCODER_LED_R_PIN, c.R);
		analogWrite(X_ENCODER_LED_G_PIN, c.G);
		analogWrite(X_ENCODER_LED_B_PIN, c.B);
		break;
	case LED_Y:
		analogWrite(Y_ENCODER_LED_R_PIN, c.R);
		analogWrite(Y_ENCODER_LED_G_PIN, c.G);
		analogWrite(Y_ENCODER_LED_B_PIN, c.B);
		break;
	case LEDS:
		analogWrite(X_ENCODER_LED_R_PIN, c.R);
		analogWrite(X_ENCODER_LED_G_PIN, c.G);
		analogWrite(X_ENCODER_LED_B_PIN, c.B);
		analogWrite(Y_ENCODER_LED_R_PIN, c.R);
		analogWrite(Y_ENCODER_LED_G_PIN, c.G);
		analogWrite(Y_ENCODER_LED_B_PIN, c.B);
		break;
	default:
		analogWrite(X_ENCODER_LED_R_PIN, 255);
		analogWrite(X_ENCODER_LED_G_PIN, 255);
		analogWrite(X_ENCODER_LED_B_PIN, 255);
		analogWrite(Y_ENCODER_LED_R_PIN, 255);
		analogWrite(Y_ENCODER_LED_G_PIN, 255);
		analogWrite(Y_ENCODER_LED_B_PIN, 255);
	}
}


/* ISR's */
void x_enc_rotate() {
	unsigned char result = x_enc.process();
	if (result == DIR_CW) {
		x_encPos++;
	}
	else if (result == DIR_CCW) {
		x_encPos--;
	}
}

void y_enc_rotate() {
	unsigned char result = y_enc.process();
	if (result == DIR_CW) {
		y_encPos++;
	}
	else if (result == DIR_CCW) {
		y_encPos--;
	}
}