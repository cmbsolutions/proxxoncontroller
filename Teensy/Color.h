#pragma once
#define LED_RED color(255, 0, 0)	
#define LED_ORANGE color(255, 128, 0)
#define LED_YELLOW color(255, 255, 0)
#define LED_LIME color(128, 255, 0)
#define LED_GREEN color(0, 255, 0)	
#define LED_AQUAMARINE color(0, 255, 128)
#define LED_CYAN color(0, 255, 255)
#define LED_DEEPSKYBLUE color(0, 128, 255)	
#define LED_BLUE color(0, 0, 255)	
#define LED_PURPLE color(128, 0, 255)
#define LED_MAGENTA color(255, 0, 255)
#define LED_FUSCIA color(255, 0, 128)
#define LED_BLACK color(0,0,0)

struct color {
	byte R;
	byte G;
	byte B;
	color(byte red, byte green, byte blue) { R = 255 - red; G = 255 - green; B = 255 - blue; }
};