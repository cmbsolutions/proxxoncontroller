# ProxxonController

A controller for a manual CNC machine by proxxon, the BFW40
This controller will convert the manual X and Y axis to steppermotor powered.
The Z-axis still is manual, mainly because i didnt find a good way to place a steppermotor there.

The controller consists of:

* 1x PJRC's Teensy 3.1 (is 5v compatible on all ports)
+ 2x polulu A4988 stepper controllers
- 2x Sparkfun rotary encoders with pushbutton and RGB led
* 2x transparent knobs for the encoders
+ 1x 2.4" Nextion smart display
- 1x 12v powerjack
* 1x LM7805 regulator (5v 1A)
+ 4x plastic M3 boardspacers 25mm
- 6x Jumpers to select stepsize
* some capacitors, resistors, pinheaders (male and female) and some wire


## Project structure
- Teensy: sourcecode for teensy 3.1
- Nextion: Nextion HMI project including images and fonts used by the project
- STL: 3D files in .STL format and technical drawings of the conversion parts needed to connect the steppermotors to the X and Y axis.
- Eagle: Schematic, board layout and libraries needed to build/change the controller
- Doc: Some documentation etc.

## Images
### Proxxon BFW40
- - - -
![alt text](https://bitbucket.org/cmbsolutions/proxxoncontroller/raw/master/Doc/proxxonbfw40e.jpg "Original Proxxon BFW40")


### Eagle schema and board of controller
- - - -
![alt text](https://bitbucket.org/cmbsolutions/proxxoncontroller/raw/master/Doc/eagle_schematic.png "Eagle schema")

![alt text](https://bitbucket.org/cmbsolutions/proxxoncontroller/raw/master/Doc/eagle_board.png "Eagle board")

- - - -
#### Tip
If you don't have the ability to make a 2 layered pcb (top and bottom) just create the bottom and put wires on the top (all red lines on the board image can be wires)
